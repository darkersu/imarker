---
layout: home

title: IT-Maker
titleTemplate: 前端永不褪色

hero:
  name: IT-Maker
  text: 技术永不褪色
  tagline: 挖掘资源，深耕技术，输出成果。
  image:
    src: /logo.svg
    alt: Skytech Design
  actions:
    - theme: brand
      text: Get Started
      link: /guide/
    - theme: alt
      text: Why ElementPlus Intensifier?
      link: /guide/why
    - theme: alt
      text: View on GitHub
      link: https://github.com/vitejs/vite

features:

- icon: 💡
  title: 代码简洁
  details: 极大的改善了组件代码堆积成山的情况，一步到位，彻底摆脱冗余代码！
- icon: ⚡️
  title: 快速开发
  details: 让研发人员更加关注业务，大量减少重复代码的劳动！
- icon: 🛠️
  title: 功能增强
  details: 增强组件功能，让组件更加贴近我们的业务！
- icon: 📦
  title: 文献参考更加丝滑
  details: 所有增强组件参考文献均以 Element-Plus 官方文档为主，做到丝滑过度，无需考虑多份文档切换！
- icon: 🔩
  title: 示例场景更加丰富
  details: 提供更多丰富的示例场景，同时我们依据业务背景，会不定期丰富我们的示例场景，做更多的场景尝试！
- icon: 🔑
  title: 扩展功能更加强大
  details: 我们基于组件库做增强，但不限于组件增强；我们尝试做更多的前端功能扩展！
---
