# 文档市场
收录前端各类常用文档及对应中文文档地址，方便开发过程中权威资料查询
## Basic - 前端基础
### MDN
官网：[https://www.typescriptlang.org/](https://www.typescriptlang.org/)
### TypeScript
官网：[https://www.typescriptlang.org/](https://www.typescriptlang.org/)

中文网站：[https://www.tslang.cn/index.html](https://www.tslang.cn/index.html)

## 构建工具
### Webpack
  - 英文官网：[https://webpack.js.org/](https://webpack.js.org/)
  - 中文文档：[https://www.webpackjs.com/](https://www.webpackjs.com/)


### Vite
  - 英文文档：[https://vitejs.dev/](https://vitejs.dev/)
  - 中文文档：[https://cn.vitejs.dev/](https://cn.vitejs.dev/)

## 小程序
- 微信：[https://developers.weixin.qq.com/miniprogram](https://developers.weixin.qq.com/miniprogram/dev/framework/)
- 支付宝：[https://opendocs.alipay.com/mini/developer](https://opendocs.alipay.com/mini/developer)

## uni-app
- 文档中心：[https://uniapp.dcloud.net.cn/](https://uniapp.dcloud.net.cn/)


## SSR
### Nuxt
- Nuxt2
  - 官网：[https://nuxtjs.org/](https://nuxtjs.org/)
  - 中文文档：[https://www.nuxtjs.cn/](https://www.nuxtjs.cn/)
- Nuxt3：
  - 官网：[https://nuxt.com/blog/v3](https://nuxt.com/blog/v3)
  - 中文文档：[https://www.nuxt.com.cn/](https://www.nuxt.com.cn/)
  - 中文文档（个人翻译）：[码农叉叉歪](https://xxy5.com/nuxt3/introduction-install.html)
### Next
- 英文官网：[https://nextjs.org/](https://nextjs.org/)
- 中文文档：[https://www.nextjs.cn/](https://www.nextjs.cn/)

## UI 组件库
### Element
  - Element-ui（Vue2）：[ElementUI](https://element.eleme.cn/#/zh-CN)
  - Element-Plus（Vue3）：[ElementPlus](https://element-plus.gitee.io/zh-CN/)

### Ant Design
  - Ant Design for React：[React版本](https://ant.design/docs/react/introduce-cn)
  - Ant Design for Vue：
    - [Vue2](https://1x.antdv.com)
    - [Vue3](https://2x.antdv.com)
  - Ant Design for Mobile：[Mobile版本](https://mobile.ant.design/zh)


### iView
  - iView2（Vue2）：[http://v4.iviewui.com/](http://v4.iviewui.com/)
  - iView3（Vue3）：[https://www.iviewui.com](https://www.iviewui.com/view-ui-plus/guide/introduce)

### Vant
  - Vant2：[Vue2 版本](https://vant-contrib.gitee.io/vant/v2/#/zh-CN/)
  - Vant4：[Vue3 版本](https://vant-contrib.gitee.io/vant/#/zh-CN)
  - Vant3：[React 版本](https://react-vant.3lang.dev/)
  - Vant Weapp：[Vant微信小程序](https://youzan.github.io/vant-weapp/#/home)
  - Vant ailapp：[Vant支付宝小程序](https://ant-move.github.io/vant-ailapp-docs)
### uView
- uView UI：[uni-app组件库](https://www.uviewui.com/)

## 图表支持
### Echarts
 官网：[文档中心](https://echarts.apache.org/zh/option.html)
-  示例Demo：
   - 示例文档：[官网示例中心](https://echarts.apache.org/examples/zh/index.html)
   - 个人示例Demo1：[Echarts社区](https://www.makeapie.cn/echarts)
   - 个人示例Demo2：[ISQQW Echart](https://www.isqqw.com/) 

更多示例Demo参考文章“[**全网echarts案例资源大总结和echarts的高效使用技巧（细节版）**](https://juejin.cn/post/7078834647005822983)”
### D3.js
官网：[https://d3js.org/](https://d3js.org/)

中文文档：[https://www.d3js.org.cn](https://www.d3js.org.cn/document/)

## 其他
### Pinia
官网地址：[https://pinia.vuejs.org/](https://pinia.vuejs.org/zh/)

### VueX
官网地址：[https://vuex.vuejs.org/zh/](https://vuex.vuejs.org/zh/)
### Vuepress
  VuePress文档：[https://vuepress.vuejs.org/](https://vuepress.vuejs.org/)
  
  中文文档：[个人翻译](http://www.fenovice.com/doc/vuepress-next/)
### Vitepress
- Vitepress官网：[https://vitepress.dev/](https://vitepress.dev/)
- Vitepress中文文档（个人翻译）：[码农叉叉歪](https://xxy5.com/vitepress-cn/)
### Electron
- 文档中心：[https://www.electronjs.org/](https://www.electronjs.org/)
### Three.js
官网文档：[https://threejs.org/](https://threejs.org/) 

中文网站：[http://www.webgl3d.cn/](http://www.webgl3d.cn/)