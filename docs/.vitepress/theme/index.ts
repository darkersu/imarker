import defaultTheme from 'vitepress/dist/client/theme-default/index';
import '../styles/vars.css'
export default {
    ...defaultTheme
}